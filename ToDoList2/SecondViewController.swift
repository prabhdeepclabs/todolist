//
//  SecondViewController.swift
//  ToDoList2
//
//  Created by Bhasker on 1/20/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var toDoItem: UITextField! // text field for new to do item to be added
    
    // add item button pressed
    @IBAction func addItem(sender: AnyObject) {
        // add the new item to the ToDoItemss array and also update in permanent storage using persistent storage
        
        if ( toDoItem.text == "") {
            // if text field left blank and button pressed do nothing
            
            return()
        }
        
        toDoItems.append( toDoItem.text )
        
        let fixedToDoItems = toDoItems // to make it immutable
        NSUserDefaults.standardUserDefaults().setObject(fixedToDoItems, forKey: "toDoItems")
        NSUserDefaults.standardUserDefaults().synchronize()
    
        var storedToDoItems : AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey("toDoItems")
        println( storedToDoItems )
        
        self.view.endEditing(true) // hide keyboard
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // to hide keyboard on return key pressed
        
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        // to hide keyboard when clicked anywhere on screen
        
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

