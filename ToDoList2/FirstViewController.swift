//
//  FirstViewController.swift
//  ToDoList2
//
//  Created by Bhasker on 1/20/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

var toDoItems : [String] = [] // create global toDoItems array

class FirstViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tasksTable : UITableView! // reference made to TableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // number of rows in TableView
        
        return toDoItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // define and return the cells in the TableView
        
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        cell.textLabel?.text = toDoItems[indexPath.row]
        return cell
    }
    
    override func viewWillAppear(animated: Bool) {
        // to refresh the table whenever tab opened
        
        if var storedToDoItems : AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("toDoItems") {
            // load the list of ToDoItems from the permanent storage if any
            
            toDoItems = []
            
            for var i = 0 ; i < storedToDoItems.count ; ++i {
                
                toDoItems.append(storedToDoItems[i] as NSString)
            }
        }
        tasksTable.reloadData() // refresh table
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // to delete a row i.e a to do item from table and update the permanent storage as well
        
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            toDoItems.removeAtIndex(indexPath.row)
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic) // updates table
            
            let fixedToDoItems = toDoItems
            NSUserDefaults.standardUserDefaults().setObject(fixedToDoItems, forKey: "toDoItems")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

